import { Injectable } from "@angular/core";
import { Restaurante } from "./restaurante.model";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { BehaviorSubject } from "rxjs";
import { map, tap } from "rxjs/operators";


@Injectable({
 providedIn: 'root'
})
export class RestauranteService{

constructor(private http: HttpClient){}

 private _restaurantes = new BehaviorSubject<Restaurante[]>([]);

//  private restaurante: Restaurante[] = [
//   {id: null, titulo: 'Carls Jr', platillos: ['Hamburguesas', 'Ensaladas', 'Helados'],
//   imgUrl:'https://frankata.com/wp-content/uploads/2019/01/800x600carlsjr-1170x877.jpg'},
//   {id: null, titulo: 'Cabo Grill', platillos: ['Ceviche', 'Filete de pescado', 'Tacos de mariscos'],
//   imgUrl:'https://i.pinimg.com/280x280_RS/e3/1e/e7/e31ee7950607eb55c87e199fd5ab6dd7.jpg' },
//   {id: null, titulo: 'Super Salads', platillos: ['Ensaladas', 'Wraps', 'Paninis'],
//   imgUrl:'https://cdn.worldvectorlogo.com/logos/super-salads.svg'},
//   ];


 get restaurantes(){
  return this._restaurantes.asObservable();
  }

 addRestaurante(restaurante: Restaurante){
  this.http.post<any>(environment.firebaseUrl + 'restaurantes.json', {...restaurante}).subscribe(data => {
    console.log(data);
    });
    }

    fetchRestaurantes(){

      /*this.addRestaurante(this.restaurante[0]);
      this.addRestaurante(this.restaurante[1]);
      this.addRestaurante(this.restaurante[2]);*/
      return this.http.get<{[key: string] : Restaurante}>(
        environment.firebaseUrl + 'restaurantes.json'
      )
      .pipe(map(dta =>{
        const rests = [];
        for(const key in dta){
          if(dta.hasOwnProperty(key)){
            rests.push(
              new Restaurante(key, dta[key].titulo, dta[key].imgUrl, dta[key].platillos, dta[key].lat, dta[key].lng
            ));
          }
        }
        return rests;
      }),
      tap(rest => {
      this._restaurantes.next(rest);
      }));
      }



 getRestautante(restauranteId: string){
  const url = environment.firebaseUrl + `restaurantes/${restauranteId}.json`;
  return this.http.get<Restaurante>(url)
  .pipe(map(dta => {
  return new Restaurante(restauranteId, dta.titulo, dta.imgUrl, dta.platillos, dta.lat, dta.lng);
  }));
 }



}
