import { BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';
import { environment } from './../../../environments/environment';
import { Ofertas } from './ofertas.model';
import { Injectable } from "@angular/core";


@Injectable({
  providedIn: 'root'
 })

 export class OfertasService {

 constructor(private http: HttpClient){}

private _ofertas = new BehaviorSubject<Ofertas[]>([]);

  private ofertas1: Ofertas[]= [
    {id: null, titulo: '2X1 EN CERVEZAS Y BEBIDAS (MARTES Y MIERCOLES) DESDE LAS 5 PM',
      imgUrl: 'https://mk0cazaofertassmxlbf.kinstacdn.com/wp-content/uploads/2014/12/cabo-nights-2x1-cervezas-cabo-grill.jpg',
      vigen: 'Solo durante el mes de marzo' },
    {id: null, titulo: 'WESTERN BACON CHEESEBURGUER CON PAPAS SOLO POR $79',
      imgUrl: 'https://promociondescuentos.com/wp-content/uploads/2018/07/promocion-wester-a-solo-79-300x200.jpg',
      vigen: 'Promoción solo valida de Lunes a Viernes hasta Julio 2021'},
    {id: null, titulo: 'COMBO FAN $79 SOPA CHICA + MEDIA ENSALADA',
      imgUrl: 'https://ik.imagekit.io/smdxc0e2g3/userscontent2-endpoint/images/c5eaef17-f2b6-45c3-8aa0-b34d03732639/bd8feba7ed0d58db7dca050416e279d0.png?tr=w-320,rt-0',
      vigen: 'Aplica solo de Lunes a Viernes (Solo el durante el mes de Marzo y Abril 2021)'},
  ];

get ofertas(){
  return this._ofertas.asObservable();
}

addOfertas(ofertas: Ofertas){
  this.http.post<any>(environment.firebaseUrl + 'ofertas.json', {...ofertas}).subscribe(data => {
    console.log(data);
  });
}

 fetchOfertas(){

  /*this.addOfertas(this.ofertas1[0]);
  this.addOfertas(this.ofertas1[1]);
  this.addOfertas(this.ofertas1[2]);*/
  return this.http.get<{[key: string] : Ofertas}>(
    environment.firebaseUrl + 'ofertas.json'
  )
  .pipe(map(dta =>{
    const rests = [];
    for(const key in dta){
      if(dta.hasOwnProperty(key)){
        rests.push(
          new Ofertas(key, dta[key].titulo, dta[key].imgUrl, dta[key].vigen
        ));
      }
    }
    return rests;
  }),
  tap(rest => {
  this._ofertas.next(rest);
  }));
  }

 getOferta(ofertasId: string){
 const url = environment.firebaseUrl + `ofertas/${ofertasId}.json`;
 return this.http.get<Ofertas>(url)
 .pipe(map(dta => {
   return new Ofertas(ofertasId, dta.titulo, dta.imgUrl, dta.vigen)
 }));
  }

 }
