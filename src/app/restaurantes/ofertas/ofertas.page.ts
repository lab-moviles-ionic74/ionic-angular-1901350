import { Subscription } from 'rxjs';
import { MenuController } from '@ionic/angular';
import { Ofertas } from './ofertas.model';
import { Component, OnInit } from '@angular/core';
import{ OfertasService} from './ofertas.service';


@Component({
  selector: 'app-ofertas',
  templateUrl: './ofertas.page.html',
  styleUrls: ['./ofertas.page.scss'],
})
export class OfertasPage implements OnInit {

  ofertas: Ofertas[];
  ofertasSub: Subscription;
  isLoading = false;

  constructor(private ofertasService: OfertasService, private menuCtrl: MenuController) { }

  ngOnInit() {
    console.log('ANGULAR -> ngOnInit');
    this.ofertasSub = this.ofertasService.ofertas.subscribe(rests => {
      this.ofertas = rests;
    });
  }

ionViewWillEnter(){
 console.log('IONIC -> ionViewWillEnter');
 this.isLoading = true;
 this.ofertasSub = this.ofertasService.fetchOfertas().subscribe(() => {
   this.isLoading = false;
 });
}

  ionViewDidEnter(){
   console.log('IONIC -> ionViewDidEnter');
  }

ionViewWillLeave(){
 console.log('IONIC -> ionViewWillLeave');
}

ionViewDidLeave(){
 console.log('IONIC -> ionViewDidLeave');
}

ngOnDestroy(){
  console.log('ANGULAR -> ngOnDestroy');
  if(this.ofertasSub){
    this.ofertasSub.unsubscribe();
  }
}

openSideMenu(){
  this.menuCtrl.open();
}

}
