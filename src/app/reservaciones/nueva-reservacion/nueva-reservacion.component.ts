import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Restaurante } from 'src/app/restaurantes/restaurante.model';
import { NgForm } from '@angular/forms';
import { DatePipe } from '@angular/common';



@Component({
  selector: 'app-nueva-reservacion',
  templateUrl: './nueva-reservacion.component.html',
  styleUrls: ['./nueva-reservacion.component.scss'],
  providers: [DatePipe]
})
export class NuevaReservacionComponent implements OnInit {

  @Input() restaurante: Restaurante;
  @Input() mode: 'select' | 'hoy';
  @ViewChild('formNew') myForm: NgForm;
  fecha: string;
  Nombre: string;
  desdeMin: string;
  opciones: any={ year: 'numeric', month: 'long', day: 'numeric'};

  constructor(
    private modalCtrl: ModalController,
  ) {}

  ngOnInit() {
    const hoy = new Date();
    this.desdeMin = hoy.toISOString();
    if(this.mode === 'hoy'){
    this.fecha = hoy.toISOString();
    }
  }

  onReservar(){
    this.modalCtrl.dismiss({
      restaurante: this.restaurante,
      nombre: this.myForm.value['nombre'],
      horario: new Date(this.myForm.value['horario']).toLocaleDateString('es-ES', this.opciones)},
      'confirm');

  }

  onCancel(){
    this.modalCtrl.dismiss(null, 'cancel');
   }


}
