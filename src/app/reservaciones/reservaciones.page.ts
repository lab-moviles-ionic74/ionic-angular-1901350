import { ReservacionesService } from './reservaciones.service';
import { Reservaciones } from './reservaciones.model';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { IonItemSliding, LoadingController } from '@ionic/angular';


@Component({
  selector: 'app-reservaciones',
  templateUrl: './reservaciones.page.html',
  styleUrls: ['./reservaciones.page.scss'],
})
export class ReservacionesPage implements OnInit {

  reservaciones:Reservaciones[]=[];
  reservacionesSub: Subscription;
  isLoading = false;

  constructor(
    private reservacionesService: ReservacionesService,
    private loadingCtrl: LoadingController
  ) { }

  ngOnInit() {
    this.reservacionesSub = this.reservacionesService.reservaciones.subscribe(rsvs => {
      this.reservaciones = rsvs;
    });
  }

  ionViewWillEnter(){
      console.log('IONIC -> ionViewWillEnter');
      this.isLoading = true;

      this.reservacionesSub = this.reservacionesService.fetchReservaciones().subscribe(rsvs => {
        this.reservaciones = rsvs;
        this.isLoading = false;
      });
  }

  ngOnDestroy(){
      if(this.reservacionesSub){
       this.reservacionesSub.unsubscribe();
      }
  }

  onRemoveReservacion(reservacionId: string, slidingEl: IonItemSliding){
      slidingEl.close();
      this.loadingCtrl.create({
        message: 'eliminando reservación ...'
      })
      .then(loadingEl => {
        loadingEl.present();
        this.reservacionesService.removeReservacion(reservacionId).subscribe(() => {
          loadingEl.dismiss();
        });
      });
    }

}
