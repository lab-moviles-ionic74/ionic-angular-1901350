
import { Restaurante } from 'src/app/restaurantes/restaurante.model';
import { Reservaciones } from './reservaciones.model';
import { Injectable } from "@angular/core";
import { BehaviorSubject } from 'rxjs';
import { map, tap, take, switchMap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { LoginService } from '../login/login.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
 })

 export class ReservacionesService {
  private _reservaciones = new BehaviorSubject<Reservaciones[]>([]);
  usuarioId = null;

  get reservaciones(){
  return this._reservaciones.asObservable();
  }

  fetchReservaciones(){
  return this.http.get<{[key: string] : Reservaciones}>(
    environment.firebaseUrl + 'reservaciones.json?orderBy="usuarioId"&equalTo="'+ this.usuarioId + '"'
  )
  .pipe(map(dta =>{
    const rests = [];
    for(const key in dta){
      if(dta.hasOwnProperty(key)){
          rests.push(new Reservaciones(
            key,
            dta[key].restauranteId,
            dta[key].restaurante,
            dta[key].nombre,
            dta[key].horario,
            dta[key].imgUrl,
            dta[key].usuarioId
        ));
      }
    }
    return rests;
  }),
  tap(rest => {
  this._reservaciones.next(rest);
  }));
  }

  addReservacion(restaurante: Restaurante, nombre: string, horario: string){
  const rsv = new Reservaciones(
    null,
    restaurante.id,
    restaurante.titulo,
    nombre,
    horario,
    restaurante.imgUrl,
    this.usuarioId
  );
  this.http.post<any>(environment.firebaseUrl + 'reservaciones.json', {...rsv}).subscribe(data => {
    console.log(data);
    });
  }

    removeReservacion(reservacionId: string){
      let url = `${environment.firebaseUrl}reservaciones/${reservacionId}.json`;
      return this.http.delete(url)
      .pipe(switchMap(()=>{
        return this.reservaciones;
      }), take(1), tap(rsvs => {
        this._reservaciones.next(rsvs.filter(r => r.id!= reservacionId))
      }))
    }

    getReservacion(reservacionId: string){
      const url = environment.firebaseUrl + `reservaciones/${reservacionId}.json`;

      return this.http.get<Reservaciones>(url)
      .pipe(map(dta => {
        return new Reservaciones(
          reservacionId,
          dta.restauranteId,
          dta.restaurante,
          dta.nombre,
          dta.horario,
          dta.imgUrl,
          dta.usuarioId
        );
      }));
    }

    updateReservacion(reservacionId: string, reservacion: Reservaciones){
      const url = environment.firebaseUrl + `reservaciones/${reservacionId}.json`;
      this.http.put<any>(url, {...reservacion}).subscribe(data => {
        console.log(data);
      });
    }


    constructor(
      private http: HttpClient,
      private loginService: LoginService
    ) {
    this.loginService.usuarioId.subscribe(usuarioId => {
    this.usuarioId = usuarioId;
    });
    }
   }
