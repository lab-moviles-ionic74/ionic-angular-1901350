import { Injectable } from '@angular/core';
import { CanLoad, Route, UrlSegment, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { LoginService } from './login.service';
import { switchMap,take, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanLoad {

  constructor(
    private loginService:LoginService,
    private router:Router
  ){}


  canLoad(
    route: Route,
    segments: UrlSegment[]): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      return this.loginService.usuarioLoggedo.pipe(
        take(1),
        switchMap(isAuth => {
          if(!isAuth){
            return this.loginService.autoLogin();
          }
          else{
            return of(isAuth);
          }
        }),
        tap(isAuth => {
          console.log(this.loginService.usuarioLoggedo);
          if(!this.loginService.usuarioLoggedo){
            this.router.navigateByUrl('/login');
          }
        })
      );
    }

}
